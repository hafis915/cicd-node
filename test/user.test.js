const app = require('../index.js')
// const { user } = require('../models')
const request = require('supertest')
const dummy1 = {
    name : 'dummyUser1',
    email : 'dummyUser1@mail.com'
}
const dummy2 = {
    name : 'dummyUser2',
    email : 'dummyUser2@mail.com'
}
const dummy3 = {
    name : 'dummyUser3',
    email : 'dummyUser3@mail.com'
}
const dummy4 = {
    name : 'dummyUser4',
    email : 'dummyUser4@mail.com'
}

let idDummy1 
async function createUser() {
    try {
        await user.bulkCreate([dummy1, dummy2])
        return true
    } catch (error) {
        console.log(error)
    }
}

async function bulkDeleteUserTable() {
    try {
        await user.destroy({where : {}})
        return true
    } catch (error) {
        console.log(error)
    }
}

beforeAll(() => {
    // return createUser()
    // akan dijalankan sebelum menjalakan testing
})

afterAll(() => {
    // return bulkDeleteUserTable()
    // akan dijalakan setelah menjalankan testing
})

describe('===== Get User ======', () => {
    it('success get all User', (done) => {
        request(app)
        .get('/user')
        .then((res) => {
            const { body, status } = res
            expect(status).toBe(200)
            // expect(body.data).toHaveLength(2)
            // expect(body.data[0].name).toBe('dummyUser1')
            // expect(body.data[0].email).toBe('dummyUser1@mail.com')
            // expect(body.data[1].name).toBe('dummyUser2')
            // expect(body.data[1].email).toBe('dummyUser2@mail.com')
            // expect(body.message).toBe('succes get all data')

            // idDummy1 = body.data[0].id
            done()
        })
    })

    it('success get one user', (done) => {
        request(app)
        .get(`/user/${idDummy1}`)
        .then(res => {
            const { body, status } = res
            // expect(status).toBe(200)
            // expect(body.data.name).toBe('dummyUser1')
            // expect(body.data.email).toBe('dummyUser1@mail.com')
            // expect(body.data.id).toBeTruthy()
            done()
        })
    })

    it('fail get user wrong url', (done) => {
        request(app)
        .get('/users')
        .then((res) => {
            const { body, status } = res
            expect(status).toBe(404)
            done()
        })
    })
})

describe('Create user', () => {
    it('success create user', (done) => {
        request(app)
        .post('/user')
        .send(dummy3)
        .then((res) => {
            const { body,status} = res
            expect(status).toBe(200)
            done()
        })
    })
})

describe('Update user', () => {
    it('success update user', (done) => {
        request(app)
        .put(`/user/${idDummy1}`)
        .send(dummy4)
        .then(res => {
            const { body, status} = res
            expect(status).toBe(200)
            done()
        })
    })
})

describe('Update user', () => {
    it('success update user', (done) => {
        request(app)
        .delete(`/user/${idDummy1}`)
        .then(res => {
            const { body, status} = res
            expect(status).toBe(200)
            done()
        })
    })
})