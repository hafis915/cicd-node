require("dotenv").config()
const express = require('express')
const app = express()
const cors = require('cors')

// const { user } = require("./models")



app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.get('/user', async (req,res) => {
    try {
        // const data = await user.findAll() 
        res.status(200).json({
            // data,
            message : 'succes get all data'
        })
    } catch (error) {
        res.status(400).json({
            data : [],
            message : 'Fail get data'
        })
    }
})

app.get('/user/:id', async(req,res) => {
    try {
        const id = req.params.id
        // const data = await user.findOne({
        //     where : {
        //         id
        //     }
        // })
        return res.status(200).json({
            // data,
            message : 'succes get data'
        })
        
    } catch (error) {
        res.status(400).json({
            data : [],
            message : 'Fail get data'
        })
    }
})

app.post('/user', async (req, res) => {
    try {
        // console.log(req.body)
        const payload = {
            name : req.body.name,
            email : req.body.email
        }
        // console.log(payload)
        // const data = await user.create(payload)
        return res.status(200).json({
            // data  ,
            message : 'succes get data'
        })
    } catch (error) {
        return res.status(400).json({
            data : [],
            message : 'Fail get data'
        })
    }
})

app.put('/user/:id', async(req,res) => {
    try {
        const payload = {
            name : req.body.name,
            email : req.body.email
        }
        // const data = await user.update(payload, {
        //     where : {
        //         id : req.params.id
        //     }
        // })
        return res.status(200).json({
            // data,
            message : 'succes get data'
        })
    } catch (error) {
        // console.log(error)
        return res.status(400).json({
            data : [],
            message : 'Fail get data'
        })
    }
})

app.delete('/user/:id', async (req,res) => {
    try {
        const id = req.params.id
        // await user.destroy({
        //     where : {
        //         id
        //     }
        // })
        return res.status(200).json({
            message : 'succes get data'
        })
    } catch (error) {
        // console.log(error)
        return res.status(400).json({
            data : [],
            message : 'Fail get data'
        })
    }
})

console.log(process.env.NODE_ENV , "<< Ini node env")
if(process.env.NODE_ENV !=='test') {
    app.listen(5173,   () => {
        console.log("SUKSES")
    })
}


module.exports = app